<?php
    include("../functions/generics.php");

    if(isset($_POST['id_delete'])) {
        $db = dbConnectPdo();

        $query = "DELETE FROM `SESSIONS` WHERE id = :id";
        
        $stm = $db->prepare($query);

        $stm->bindParam(':id', $_POST['id_delete']);

        $stm->execute();

        $db = null;
        Header('Location: http://localhost:80/?page=activities');
    }
?>