<?php
    include("../functions/generics.php");

    $db = dbConnectPdo();

    if(count($_POST) > 0) {
        $query = "INSERT INTO tutoseu.`USERS` (first_name, last_name, address, birthday, register_date) VALUES (?, ?, ?, ?, ?)";
        
        $db->prepare($query)->execute([$_POST['first_name'], $_POST['last_name'], $_POST['address'], $_POST['birthday'], gmdate('Y-m-d h:i:s', time() + 3600)]);

        // $date = gmdate('Y-m-d h:i:s', time() + 3600);
        // $query = "INSERT INTO tutoseu.`USERS` (first_name, last_name, address, birthday, register_date) VALUES ('{$_POST['first_name']}', '{$_POST['last_name']}', '{$_POST['address']}', '{$_POST['birthday']}', '{$date}')";
        // $db->query($query)->exec();
    }

    $db = null;
    Header('Location: http://localhost:80/?page=home');
?>

