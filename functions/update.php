<?php
    include("../functions/generics.php");

    if(isset($_POST['id_modify'])) {
        $db = dbConnectPdo();

        $query = "UPDATE `SESSIONS` SET note = :note WHERE id = :id";
        
        $stm = $db->prepare($query);

        $stm->bindParam(':note', $_POST['note_modify']);
        $stm->bindParam(':id', $_POST['id_modify']);

        $stm->execute();

        $db = null;
        Header('Location: http://localhost:80/?page=activities');
    }
?>