<?php
    function dbConnect(){
        $host = 'db'; //Nom donné dans le docker-compose.yml
        $user = 'root'; // user et pwd du docker compose
        $password = 'root';
        $db = 'tutoseu';
        $conn = new mysqli($host,$user,$password,$db);
        
        if(!$conn) {
            return false;
        } else{
            return $conn;
        }
    }

    function dbConnectPdo(){
        $user = 'root'; // user et pwd du docker compose
        $password = 'root';

        try {
            $db = new PDO('mysql:host=db;dbname=tutoseu', $user, $password);
            if($db) return $db;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    function removeExtentionName($file) {
        $current_file = substr($file, 0, strrpos($file, "."));
        return $current_file;
    }


?>