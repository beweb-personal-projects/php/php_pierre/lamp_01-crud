<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand ms-1" href="http://localhost:80/?page=home">CRUD PROJECT</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<?php
				$pages = scandir("./views/");

				for ($i=2; $i < count($pages); $i++): ?>
					<li class="nav-item">
						<a class="nav-link" href="http://localhost:80/?page=<?=removeExtentionName($pages[$i])?>"><?=ucfirst(removeExtentionName($pages[$i]))?></a>
					</li>
				<?php endfor
			?>
		</ul>
	</div>
</nav>