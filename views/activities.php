<!-- MYSQLI OBJECT MODE -->

<!-- <div class="container-fluid mt-4 mb-4 text-center">
    <h3>MYSQLI OBJECT MODE</h3>
</div>

<div class="container">
    <table class="table table-dark table-striped table-hover text-center">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">DESIGNATION</th>
            <th scope="col">PARTICIPATION PRICE</th>
            <th scope="col">ORGANIZATION PRICE</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                if(dbConnect()) {
                    $db = dbConnect();
                    
                    $query_activities = "SELECT * FROM `ACTIVITIES`";

                    foreach ($db->query($query_activities) as $row):?>
                        <tr>
                            <th scope="row"><?=$row['id']?></th>
                            <td><?=$row['designation']?></td>
                            <td><?=$row['participation_price']?>€</td>
                            <td><?=$row['organization_price']?>€</td>
                        </tr>
                    <?php endforeach;
                    
                    $db->close();
                }
            ?>
        </tbody>
    </table>
</div> -->

<!-- PDO MODE -->

<div class="container mt-3">
    <table class="table table-striped table-hover text-center">
        <thead>
            <tr class="bg-primary text-light">
                <th scope="col">ID</th>
                <th scope="col">ACTIVITY</th>
                <th scope="col">DATE SESSION</th>
                <th scope="col">NOTE</th>
                <th scope="col" colspan="2">ACTIONS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                if(dbConnectPdo()) {
                    $db = dbConnectPdo();
                    
                    $query_activities = "   SELECT SESSIONS.id, SESSIONS.date, SESSIONS.note, ACTIVITIES.designation as designation, SESSIONS.id_activity 
                                            FROM `SESSIONS` 
                                            INNER JOIN `ACTIVITIES` 
                                            ON SESSIONS.id_activity = ACTIVITIES.id 
                                            ORDER BY SESSIONS.id ASC ";

                    foreach ($db->query($query_activities) as $row):?>
                        <tr>
                            <form method="POST">
                                <th>
                                    <?=$row['id']?>
                                    <input type="text" name="id" value="<?=$row['id']?>" hidden>
                                </th>
                                <td>
                                    <?=$row['designation']?>
                                    <input type="text" name="designation" value="<?=$row['id_activity']?>" hidden>
                                </td>
                                <td>
                                    <?=$row['date']?>
                                    <input type="text" name="date" value="<?=$row['date']?>" hidden>
                                </td>
                                <td>
                                    <?=$row['note']?>
                                    <input type="text" name="note" value="<?=$row['note']?>" hidden>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </td>
                            </form>
                            <form method="POST" action="http://localhost:80/functions/delete.php">
                                <td>
                                    <input type="text" name="id_delete" value="<?=$row['id']?>" hidden>    
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                </td>
                            </form>
                        </tr>
                        <?php endforeach;
                    
                    if(isset($_POST['id'])):?>
                        <tr>
                            <form method="POST" action="http://localhost:80/functions/update.php">
                                <th scope="row" readonly>
                                    <?=$_POST['id']?>
                                    <input type="text" name="id_modify" value="<?=$_POST['id']?>" hidden>
                                </th>
                                <td>
                                    <?=$_POST['designation']?>
                                    <input type="text" name="designation_modify" value="<?=$_POST['designation']?>" hidden>
                                </td>
                                <td>
                                    <?=$_POST['date']?>
                                    <input type="text" name="date_modify" value="<?=$_POST['date']?>" hidden>
                                </td>
                                <td>
                                    <textarea class="form-control" rows="3" name="note_modify"><?=$_POST['note']?></textarea>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-warning" value="Submit">
                                </td>
                            </form>
                        </tr>
                    <?php endif;

                    $db = null;
                }
            ?>
        </tbody>
    </table>
</div>
