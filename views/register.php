<div class="container">
    <form class="m-5 text-center" method="POST" action="http://localhost:80/functions/register.php/">
      <div class="form-group mt-2 mb-2">
        <label for="exampleInputFirstName">First Name</label>
        <input type="text" name="first_name" class="form-control text-center" id="exampleInputFirstName" placeholder="Enter your First name">
      </div>

      <div class="form-group mt-2 mb-2">
        <label for="exampleInputLastName">Last Name</label>
        <input type="text" name="last_name" class="form-control text-center" id="exampleInputLastName" placeholder="Enter your Last name">
      </div>

      <div class="form-group mt-2 mb-2">
        <label for="exampleInputAddress">Address</label>
        <input type="text" name="address" class="form-control text-center" id="exampleInputAddress" placeholder="Enter your Address">
      </div>

      <div class="form-group mt-2 mb-2">
        <label for="exampleInputBirthday">Birthday</label>
        <input type="date" name="birthday" class="form-control text-center" id="exampleInputBirthday">
      </div>

      <button type="submit" class="btn btn-primary w-100 mt-2 mb-2">Submit</button>
    </form>
</div>