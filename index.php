<!-- Generic use functions -->
<?php include("functions/generics.php");?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>CRUD PROJECT</title>
		<?php include("./components/links.php");?>
	</head>
	<body>
		<?php include("components/navbar.php")?>

		<?php 
			if(isset($_GET['page'])) {
				if(is_file("./views/" . strtolower($_GET['page']) . ".php")) {
					include("./views/" . strtolower($_GET['page']) . ".php");
				} else {
					echo "<h2 class='text-center m-5'>ERROR 404 - NOT FOUND</h2>";
				}
			} else {
				include("./views/home.php");
			}
		?>
	</body>
</html>